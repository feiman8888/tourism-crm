<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->

    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        .required{color:#FF9966}
        .thumbImg{height:200px;}
    </style>
    
    <link rel="stylesheet" href="/Public/statics/layui/css/layui.css"  media="all">
    <style>
        th{text-align:center;}
    </style>

</head>
<body>

    <div class="page-header"><h1><i class="fa fa-home"></i> 首页>产品管理>线路管理</h1></div>
     <div class="container-fluid">
         <div class="tabbable">
             <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="myTab">
                 <li <?php if($post['status'] == 1): ?>class="active"<?php endif; ?> ><a href="javascript:checkStatus(1);" >已上架</a></li>
                 <li <?php if($post['status'] == 0): ?>class="active"<?php endif; ?>><a href="javascript:checkStatus(0);" >待审核
                     <?php if($waitAudit): ?><span class="badge"><?php echo ($waitAudit); ?></span><?php endif; ?>
                    </a></li>
                 <li <?php if($post['status'] == -2): ?>class="active"<?php endif; ?>><a href="javascript:checkStatus(-2);" >被拒线路</a></li>
                 <li <?php if($post['status'] == -1): ?>class="active"<?php endif; ?>><a href="javascript:checkStatus(-1);" >下架线路</a></li>
             </ul>
             <div class="tab-content">
                 <div class="row" >
                     <form action="" class="form-inline " method="get" style="margin-left:10px;" >
                         <input type="hidden" name="status" value="<?php echo ((isset($post["status"]) && ($post["status"] !== ""))?($post["status"]):0); ?>" style="display: none;">
                         <div class="form-group">
                             <label for="lineNum">线路编号</label>
                             <input type="text" class="" name="line_sn" id="line_sn" placeholder="线路编号" value="<?php echo ($post["line_sn"]); ?>">
                         </div>
                         <div class="form-group">
                             <label for="lineName">线路名称</label>
                             <input type="text" class="" name="line_name" id="line_name" placeholder="线路名称" value="<?php echo ($post["line_name"]); ?>">
                         </div>
                        <!-- <div class="form-group">
                             <label for="pushDepartment">发布部门</label>
                             <input type="text" class="" name="department" id="department" placeholder="发布部门" value="<?php echo ($post["department"]); ?>">
                         </div>-->
                         <div class="form-group">
                             <label for="startDate">发布时间</label>
                             <input type="text" class="" name="start_time" id="start_time" placeholder="开始时间" value="<?php echo ($post["start_time"]); ?>" readonly>
                         </div>
                         <div class="form-group">
                             <label for="endDate">&nbsp;</label>
                             <input type="text" class="" name="end_time" id="end_time" placeholder="结束时间" value="<?php echo ($post["end_time"]); ?>" readonly>
                         </div>
                         <button type="submit" class="btn btn-primary">查询</button>
                     </form>
                 </div>

                 <table class="table table-striped table-bordered table-hover table-condensed text-center">
                     <tbody>
                         <tr>
                             <th>全选 <input type="checkbox" class="checkAll"></th>
                             <th>线路编号</th>
                             <th>线路名称</th>
                             <th>给运营商结算单价（<?php echo ($supplierInfo["currency"]); ?>）</th>
                             <th>出发地</th>
                             <th>发布时间</th>
                             <th>上架状态</th>
                             <th>发布人</th>
                             <th>操作</th>
                         </tr>
                         <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                             <td><input type="checkbox" class="chk" value=""></td>
                             <td><?php echo ($vo["line_sn"]); ?></td>
                             <td><?php echo ($vo["line_name"]); ?></td>

                             <td>成人:<?php echo ($vo['adult_money'] - $vo['adult_cost']); ?>
                                 儿童:<?php echo ($vo['child_money']-$vo['child_cost']); ?>
                                <!-- 老人:<?php echo ($vo['oldman_money']-$vo['oldman_cost']); ?>-->
                             </td>
                             <td><?php echo ($vo["areaName"]); ?></td>
                             <td> <?php echo (date("Y-m-d",$vo["create_time"])); ?></td>
                             <td>
                                 <?php switch($vo["line_status"]): case "1": ?>已上架<?php break;?>
                                     <?php case "0": ?>待审核<?php break;?>
                                     <?php case "-1": ?>已下架<?php break;?>
                                     <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                             </td>
                             <td><?php echo ($vo["supplier_name"]); ?></td>
                             <td>
                                 <?php if(checkAuth('Dealer/Product/lineDetail')): ?><a href="<?php echo U('lineDetail',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">详情</a><?php endif; ?>
                                 <?php switch($vo["line_status"]): case "1": if(checkAuth('Dealer/Product/soldOut')): ?><a href="<?php echo U('soldOut',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">下架</a><?php endif; ?>
                                         <?php if(checkAuth('Dealer/Product/partnersByLineId')): ?><a href="<?php echo U('partnersByLineId',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">合作运营商</a><?php endif; break;?>
                                     <?php case "0": if(checkAuth('Dealer/Product/auditLine')): ?><a href="<?php echo U('auditLine',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">审核</a><?php endif; break;?>
                                     <?php case "-2": if(checkAuth('Dealer/EditLine/form_step1')): ?><a href="<?php echo U('EditLine/form_step1',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">编辑</a><?php endif; break;?>
                                     <?php case "-1": if(checkAuth('Dealer/Product/auditLine')): ?><a href="<?php echo U('putaway',array('id'=>$vo['line_id']));?>" class="btn btn-xs btn-primary">上架</a><?php endif; break; endswitch;?>
                             </td>
                         </tr><?php endforeach; endif; else: echo "" ;endif; ?>
                            <tr>
                                <td colspan="12">
                                    <ul class="pagination">
                                        <?php echo ($show); ?>
                                    </ul>
                                </td>
                            </tr>
                     </tbody>
                 </table>
             </div>
         </div>
     </div>

<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Dealer/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>

    <script src="/Public/statics/layui/layui.all.js"></script>
    <script src="/Public/statics/layui/layui.js"></script>

    <script>
        function checkStatus(status) {
            $('input[name="status"]').val(status);
            $('form').submit();
        }

        layui.use('laydate', function() {
            var laydate = layui.laydate;
            //常规用法
            laydate.render({
                elem: '#start_time'
            });
            laydate.render({
                elem: '#end_time'
            });
        });


    </script>

<script>
    $(function () {
        var bodyH=$(document).height();
        try{
            parent.resetFrameHeight(bodyH);
        } catch(error){

        }
    })
</script>
</body>
</html>