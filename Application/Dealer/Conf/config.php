<?php
return array(
	//'配置项'=>'配置值'
	'URL_MODEL' => 2,
	'VIEW_PATH' => './Tpl/',
	'DEFAULT_THEME' => 'Dealer',
    'LOAD_EXT_CONFIG' => 'auth_config',//权限配置
	'TMPL_PARSE_STRING' => array(
        '__PUBLIC__'         => '/Public',
        '__STATIC__'         => '/Public/statics',
        '__ADMIN_ACEADMIN__' => '/Public/statics/aceadmin',
        '__PUBLIC_CSS__'     => '/Public/css',
        '__PUBLIC_JS__'      => '/Public/js',
	),
	 'SHOW_PAGE_TRACE' => false,

);
