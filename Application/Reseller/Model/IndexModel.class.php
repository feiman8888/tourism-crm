<?php
namespace Reseller\Model;

/**
 * 首页数据展示
 */
class IndexModel extends BaseModel
{

    protected $tableName = 'line_orders as l';
    /**
     * 日订单统计
     * @return    array    处理完的数据
     */
    public function dayData()
    {

        if (I('day')) {
            $start_time = strtotime(I('day')); //0点时间戳
        } else {
            $start_time = strtotime(date('Y-m-d', time())); //0点时间戳
        }
        $end_time = $start_time + 86399; //23点59分59秒
        $conf     = [
            'manager_id'   => session('reseller_user.pid') ? session('reseller_user.pid') : session('reseller_user.reseller_id'),
            'order_type'   => session('reseller_user.shop_or_reseller'),
            'create_time'  => ['between', [$start_time, $end_time]],
            'order_status' => 3,
            'is_refund'    => 0,
        ];

        $income = $this
            ->field('sum(need_pay),sum(end_need_pay)')
            ->where($conf)
            ->find();
        $income['sum(need_pay)']     = round($income['sum(need_pay)'], 2);
        $income['sum(end_need_pay)'] = round($income['sum(end_need_pay)'], 2);
        $income['profit']            = round($income['sum(end_need_pay)'] - $income['sum(need_pay)'], 2);

        $conf['order_status'] = ['in', '1,3'];
        $order['total']       = $this
            ->where($conf)
            ->getField('count(*)');

        $conf['order_status'] = 1;
        $order['wait']        = $this
            ->where($conf)
            ->getField('count(*)');
        $order['deal'] = $order['total'] - $order['wait'];
        return array_merge($order, $income);
    }

    /**
     * 月订单数据统计
     * @return [type] [description]
     */
    public function monthDate()
    {
        $time       = I('month') ? I('month') : date('Y-m', time());
        $start_time = strtotime($time . '-01');
        $s_date     = date('Y-m-d', $start_time);
        $end_time   = strtotime("$s_date +1 month") - 1;
        $conf       = [
            'manager_id'   => session('reseller_user.pid') ? session('reseller_user.pid') : session('reseller_user.reseller_id'),
            'order_type'   => session('reseller_user.shop_or_reseller'),
            'create_time'  => ['between', [$start_time, $end_time]],
            'order_status' => 3,
        ];
        $res = M('line_orders')
            ->field("FROM_UNIXTIME(create_time,'%Y-%m-%d') as day,count(*) as count,sum(total_money) as pay,sum(end_need_pay) as need")
            ->where($conf)
            ->group('day')
            ->select();
        $res     = $this->changeIndex($res);
        $end_day = date('d', strtotime("$s_date +1 month -1 day")); //获取本月最后一天
        for ($i = 1; $i <= $end_day; $i++) {
            $key = $time . '-' . str_pad($i, 2, 0, STR_PAD_LEFT);
            if (!isset($res[$key])) {
                $res[$key]['count'] = 0;
                $res[$key]['pay']   = 0;
                $res[$key]['need']  = 0;
            }
            $order['profit'][]     = round($res[$key]['need'] - $res[$key]['pay'], 2);
            $order['endNeedPay'][] = round($res[$key]['need'], 2);
            $order['orderNum'][]   = (int) $res[$key]['count'];
            $order['conf'][]       = $i . '号';
        }

        $order['profit']     = "'" . json_encode($order['profit']) . "'";
        $order['endNeedPay'] = "'" . json_encode($order['endNeedPay']) . "'";
        $order['orderNum']   = "'" . json_encode($order['orderNum']) . "'";
        $order['conf']       = "'" . json_encode($order['conf']) . "'";
        return $order;
    }

    public function yearsDate()
    {
        $time       = I('years') ? I('years') : date('Y', time());
        $start_time = strtotime($time . '-01-01');
        $s_date     = date('Y-m-d', $start_time);
        $end_time   = strtotime("$s_date +1 year") - 1;
        $conf       = [
            'manager_id'   => session('reseller_user.pid') ? session('reseller_user.pid') : session('reseller_user.reseller_id'),
            'order_type'   => session('reseller_user.shop_or_reseller'),
            'create_time'  => ['between', [$start_time, $end_time]],
            'order_status' => 3,
        ];
        $res = M('line_orders')
            ->field("FROM_UNIXTIME(create_time,'%Y-%m') as month,count(*) as count,sum(total_money) as pay,sum(end_need_pay) as need")
            ->where($conf)
            ->group('month')
            ->select();
        $res = $this->changeIndex($res);
        for ($i = 1; $i <= 12; $i++) {
            $key = $time . '-' . str_pad($i, 2, 0, STR_PAD_LEFT);
            if (!isset($res[$key])) {
                $res[$key]['count'] = 0;
                $res[$key]['pay']   = 0;
                $res[$key]['need']  = 0;
            }
            $order['profit'][]     = round($res[$key]['need'] - $res[$key]['pay'], 2);
            $order['endNeedPay'][] = round($res[$key]['pay'], 2);
            $order['orderNum'][]   = (int) $res[$key]['count'];
            $order['conf'][]       = $i . '月';
        }
        $order['profit']     = "'" . json_encode($order['profit']) . "'";
        $order['endNeedPay'] = "'" . json_encode($order['endNeedPay']) . "'";
        $order['orderNum']   = "'" . json_encode($order['orderNum']) . "'";
        $order['conf']       = "'" . json_encode($order['conf']) . "'";
        return $order;

    }

    /**
     * 首页运营商已确认的订单列表
     */
    public function orderList()
    {

        $offset = I('p') ? (I('p') - 1) * C('PAGE_SIZE') : 0;
        $data   = [];
        $conf   = [
            'order_status' => 3,
            'order_type'   => session('reseller_user.shop_or_reseller'),
            'l.manager_id' => session('reseller_user.pid') ? session('reseller_user.pid') : session('reseller_user.reseller_id'),
        ];

        $data['total']    = $this->where($conf)->count();
        $data['pageSize'] = C('PAGE_SIZE');

        $data['root'] = M('line_orders l')
            ->field('l.order_id, p.line_name, l.total_money, c.operator_account, l.create_time, l.close_time, l.order_status,need_pay')
            ->join('LEFT JOIN __PUBLIC_LINE__ p on l.line_id = p.line_id')
            ->join('LEFT JOIN __OPERATOR__ c on c.operator_id = l.verifier_id')
            ->where($conf)
            ->limit($offset, C('PAGE_SIZE'))
            ->order('order_id desc')
            ->select();
        $year          = date('Y', time()) - 24;
        $data['years'] = [];
        for ($i = 0; $i < 50; $i++) {
            $data['years'][] = $year++;
        }
        // $data['years'] = ;
        return $data;
    }

    /**
     * 订单详情
     * @return [$data] [订单详情数组]
     */
    public function orderDetail()
    {
        $data = [];
        //订单信息
        $data['order'] = M('line_orders l')
            ->field('l.*,o.line_name,g.group_num')
            ->join('LEFT JOIN __PUBLIC_LINE__ o on l.line_id = o.line_id')
            ->join('LEFT JOIN __PUBLIC_GROUP__ g on l.group_id = g.group_id')
            ->where(['l.order_id' => I('order_id')])
            ->find();
        //毛利
        $data['order']['profit'] = $data['order']['total_money'] - $data['order']['end_need_pay'];

        //发票信息
        $data['receipt'] = M('public_receipt')
            ->where(['order_id' => $data['order']['order_id']])
            ->find();

        //游客信息
        $data['tourism'] = M('public_tourists')
            ->where(['order_id' => $data['order']['order_id']])
            ->select();

        //订单联系人
        foreach ($data['tourism'] as $v) {
            if ($v['is_contact'] == 1) {
                $data['contact']['name']  = $v['tourists_name'];
                $data['contact']['phone'] = $v['tourists_phone'];
                break;
            }
        }
        switch ($data['order']['order_status']) {
            case -6:
                $data['order']['order_status_content'] = '取消';
                break;

            case -5:
                $data['order']['order_status_content'] = '已退款';
                break;

            case -4:
                $data['order']['order_status_content'] = '拒绝退款';
                break;

            case -3:
                $data['order']['order_status_content'] = '退款审核中';
                break;

            case -2:
                $data['order']['order_status_content'] = '审核不通过';
                break;

            case -1:
                $data['order']['order_status_content'] = '未支付';
                break;

            case 1:
                $data['order']['order_status_content'] = '待审核';
                break;

            case 2:
                $data['order']['order_status_content'] = '审核中';
                break;
            case 3:
                $data['order']['order_status_content'] = '已审核';
                if ($data['order']['is_refund'] == 1) {
                    $data['order']['order_status_content'] = '拒绝退款';
                }
                break;
        }

        return $data;
    }

}
