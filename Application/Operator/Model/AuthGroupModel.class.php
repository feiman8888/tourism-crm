<?php
namespace Operator\Model;
/**
 * 权限规则model
 */
class AuthGroupModel extends BaseModel{

    protected $tableName = 'operator_auth_group';
	/**
	 * 传递主键id删除数据
	 * @param  array   $map  主键id
	 * @return boolean       操作是否成功
	 */
	public function deleteData($map){
		$this->where($map)->delete();
		$group_map=array(
			'group_id'=>$map['id']
			);
		// 删除关联表中的组数据
		$result=D('AuthGroupAccess')->deleteData($group_map);
		return $result;
	}

	/**
	 * 获取运营商所有门店
	 */
	public function getAllStore(){
		$admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
		$rs = $this->where(array('admin_id'=>$admin_id,'is_store'=>1))->select();
		// dump($this->_sql());
		return $rs;
	}

	/**
	 * 根据门店用户组ID 获取员工列表
	 */
	public function getStoreListById($id){
		$list = M('operator_auth_group_access')->alias('a')
			  ->field('a.*,o.operator_id,o.operator_name')
		      ->join('INNER JOIN '.C('DB_PREFIX').'operator o ON o.operator_id=a.uid')
			  ->where(array('a.group_id'=>$id))
			  ->select();
			 // dump($list);
		return $list;
	}


}
