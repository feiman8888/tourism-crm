<?php
namespace Reseller\Model;

use Think\Model;

/**
 * 基础model
 */
class BaseModel extends Model
{
    //用于调试
    public function j($arr)
    {
        echo json_encode($arr);die;
    }
    /**
     * 提取二维数组的第一个元素，拼接成一维数组 兼容php5.4
     * @param array $arr 需要处理的二维数组
     * @return array     处理完成的一维数组
     */
    public function levelDown($arr)
    {
        return array_map('array_shift', $arr);
    }

    /**
     * 实现array_cloumn的功能
     * @param array $arr 需要处理的二维数组
     * @return array
     */
    public function array_cloumn($arr, $key)
    {
        $temp = [];
        foreach ($arr as $v) {
            $temp[] = $v[$key];
        }
        return $temp;
    }

    /**
     * 提取二维数组的第一个元素作为索引
     * @param array $arr 需要处理的二维数组
     * @return array
     */
    public function changeIndex($arr)
    {
        return array_combine(array_map('array_shift', $arr), $arr);
    }

    /**
     * 添加数据
     * @param  array $data  添加的数据
     * @return int          新增的数据id
     */
    public function addData($data)
    {
        // 去除键值首尾的空格
        foreach ($data as $k => $v) {
            $data[$k] = trim($v);
        }
        $id = $this->add($data);
        return $id;
    }

    /**
     * 修改数据
     * @param   array   $map    where语句数组形式
     * @param   array   $data   数据
     * @return  boolean         操作是否成功
     */
    public function editData($map, $data)
    {
        // 去除键值首位空格
        foreach ($data as $k => $v) {
            $data[$k] = trim($v);
        }
        $result = $this->where($map)->save($data);
        return $result;
    }

    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map)
    {
        if (empty($map)) {
            die('where为空的危险操作');
        }
        $result = $this->where($map)->delete();
        return $result;
    }

    /**
     * 数据排序
     * @param  array $data   数据源
     * @param  string $id    主键
     * @param  string $order 排序字段
     * @return boolean       操作是否成功
     */
    public function orderData($data, $id = 'id', $order = 'order_number')
    {
        foreach ($data as $k => $v) {
            $v = empty($v) ? null : $v;
            $this->where(array($id => $k))->save(array($order => $v));
        }
        return true;
    }

    /**
     * 获取全部数据
     * @param  string $type  tree获取树形结构 level获取层级结构
     * @param  string $order 排序方式
     * @return array         结构数据
     */
    public function getTreeData($type = 'tree', $order = '', $name = 'name', $child = 'id', $parent = 'pid')
    {
        // $model_name = strtolower(MODULE_NAME);
        // $pid = session("{$model_name}_user.pid")?session("{$model_name}_user.pid"):session("{$model_name}_user.{$model_name}_id");
        // $power = M( $model_name.'_auth_group' )
        //     ->field('rules')
        //     ->where(['admin_id'=>$pid])
        //     ->select();

        // 判断是否需要排序
        if (empty($order)) {
            $data = $this->select();
        } else {
            $data = $this->order($order . ' is null,' . $order)->select();
        }
        // 获取树形或者结构数据
        if ($type == 'tree') {
            $data = \Org\Nx\Data::tree($data, $name, $child, $parent);
        } elseif ($type = "level") {
            $data = \Org\Nx\Data::channelLevel($data, 0, '&nbsp;', $child);
        }
        return $data;
    }

    /**
     * 获取分页数据
     * @param  subject  $model  model对象
     * @param  array    $map    where条件
     * @param  string   $order  排序规则
     * @param  integer  $limit  每页数量
     * @param  integer  $field  $field
     * @return array            分页数据
     */
    public function getPage($model, $map, $order = '', $limit = 10, $field = '')
    {
        $count = $model
            ->where($map)
            ->count();
        $page = new_page($count, $limit);
        // 获取分页数据
        if (empty($field)) {
            $list = $model
                ->where($map)
                ->order($order)
                ->limit($page->firstRow . ',' . $page->listRows)
                ->select();
        } else {
            $list = $model
                ->field($field)
                ->where($map)
                ->order($order)
                ->limit($page->firstRow . ',' . $page->listRows)
                ->select();
        }
        $data = array(
            'data' => $list,
            'page' => $page->show(),
        );
        return $data;
    }

    /**
     * 判断是否拥有权限
     */
    public function checkAuth($mca)
    {
        if (!$mca) {
            $mca = MODULE_NAME . '/' . CONTROLLER_NAME . '/' . ACTION_NAME;
        }
        $auth   = new \Think\Auth();
        $result = $auth->check($mca, session('reseller_user.reseller_id'));
        return $result;
    }
}
