<?php
namespace Reseller\Controller;
/**
 * 后台首页控制器
 */
class IndexController extends BaseController{

    public function __construct(){
        parent::__construct();
        // dump($this->nav_data);die;
    }
    public function test($class){

    	// dump(A('OrderCenter'));
    	$method = get_class_methods(A($class));
    	$arr = ['assign','_empty','redirect','error','success','display','show','fetch','buildHtml','theme','ajaxReturn', 'get'];
    	foreach($method as $k => $v){
    		if(substr($v,0,2) != '__'
    			&& !in_array($v,$arr))
    			echo 'Reseller/',$class,'/',$v,'<hr>';
    	}
    }
	/**
	 * 首页
	 */
	public function index(){
		$this->display('index');
	}

	/**
	 * 首页订单详情
	 */
	public function orderDetail(){
		$this->ob = D('Index')->orderDetail();
		$r = D('orderCenter4')->detailView();
		$this->dataToDisplay($r,['service']);
        $this->dataToDisplay($r['service']);
		// dump(end($this->ob['tourism'])['tourists_id']);die;
		$this->display('order_detail');
	}

	/**
	 * welcome
	 */
	public function welcome(){
		$m = D('Index');
		$this->day = $m->dayData();
		$this->selectYear = I('years') ? I('years') : date('Y', time());
		if( I('years') && I('choose') ){
			$this->right = $m->yearsDate();
		}else{
			$this->right = $m->monthDate();
		}
		$list = $m->orderList();
		$pager = new \Think\Page($list['total'],$list['pageSize']);
		$list['pager'] = $pager->show();
		$this->years = $list['years'];
		$this->list = $list;
	    $this->display('welcome');
	}

}
