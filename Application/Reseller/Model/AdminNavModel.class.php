<?php
namespace Reseller\Model;
/**
 * 菜单操作model
 */
class AdminNavModel extends BaseModel{
	protected $tableName='reseller_nav';
	/**
	 * 删除数据
	 * @param	array	$map	where语句数组形式
	 * @return	boolean			操作是否成功
	 */
	public function deleteData($map){
		$count=$this
			->where(array('pid'=>$map['id']))
			->count();
		if($count!=0){
			return false;
		}
		$this->where(array($map))->delete();
		return true;
	}

	/**
	 * 获取全部菜单
	 * @param  string $type tree获取树形结构 level获取层级结构
	 * @return array       	结构数据
	 */
	public function getTreeData($type='tree',$order=''){
		// 判断是否需要排序
		if(empty($order)){
			$data=$this->where(array('platform'=>3))->select();
		}else{
			$data=$this->where(array('platform'=>3))->order('order_number is null,'.$order)->select();
		}
		// 获取树形或者结构数据
		if($type=='tree'){
			$data=\Org\Nx\Data::tree($data,'name','id','pid');
		}elseif($type="level"){
			$data=\Org\Nx\Data::channelLevel($data,0,'&nbsp;','id');
			// 显示有权限的菜单
			$auth=new \Think\Auth();
			foreach ($data as $k => $v) {
                if ($auth->check($v['mca'],session('reseller_user.reseller_id'))) {
                    // dump($v['mca']);
					foreach ($v['_data'] as $m => $n) {
						if(!$auth->check($n['mca'], session('reseller_user.reseller_id'))){
							unset($data[$k]['_data'][$m]);
						}
					}
				}else{
					// 删除无权限的菜单
					unset($data[$k]);
				}
			}
		}
        // dump($data);die;
		return $data;
	}

	/**
     * 数据排序
     * @param  array $data   数据源
     * @param  string $id    主键
     * @param  string $order 排序字段
     * @return boolean       操作是否成功
     */
    public function orderData($data,$id='id',$order='order_number'){
    	M()->startTrans();
        foreach ($data as $k => $v) {
            $v=empty($v) ? null : $v;
            $res = $this->where(array($id=>$k))->save(array($order=>$v));
            if($res === null || $res === false ) {
            	M()->rollback();
            	return false;
            }
        }
        M()->commit();
        return true;
    }
}
