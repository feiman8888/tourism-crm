<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>

    <style>
        input class="form-control"::-webkit-outer-spin-button,
        input class="form-control"::-webkit-inner-spin-button{
            -webkit-appearance: none !important;
            margin: 0;
        }
        .bloder{
            font-weight: bold;
            text-align: right;
        }
        .col-xs-12{
            margin-bottom: 10px;
        }
        .col-xs-12:after{
            content: '';
            display: table;
            height: 0;
            clear: both;
        }
    </style>


    <div class="page-header"><h1>首页 > 财务管理 &gt;营业收入总报表</h1></div>
    <div class="tab-content clearfix">
        <div class="row" >
            <form action="" id="settlement-form" class="form-inline"  style="margin-left:10px;" >
                <div class="col-xs-12">
                    <div class="col-xs-4">
                        <label for="" class="col-xs-4 bloder">团号：</label>
                        <div class="col-xs-8"><input class="form-control" type="text" name="group_num" value="<?php if($_GET['group_num']): echo ($_GET['group_num']); endif; ?>" /></div>
                    </div>
                    <div class="col-xs-4">
                        <label for="" class="col-xs-4 bloder">线路名称：</label>
                        <div class="col-xs-8"><input class="form-control" type="text" name="line_name" value="<?php if($_GET['line_name']): echo ($_GET['line_name']); endif; ?>" /></div>
                    </div>
                    <div class="col-xs-4">
                        <label for="" class="col-xs-4 bloder">运营性质：</label>
                        <div class="col-xs-8">
                            <select name="platform">
                                <option value="">全部</option>
                                <option <?php if($_GET['platform'] == 1): ?>selected<?php endif; ?> value="1">自营</option>
                                <option <?php if($_GET['platform'] == 2): ?>selected<?php endif; ?> value="2">非自营</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                        <div class="col-xs-4">
                            <label for="" class="col-xs-4 bloder">下单时间：</label>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" id="start" name="start" value="<?php if($_GET['start']): echo ($_GET['start']); endif; ?>" />
                            </div>
                            <div class="col-xs-4">
                                <input class="form-control" type="text" id="end" name="end" value="<?php if($_GET['end']): echo ($_GET['end']); endif; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-8"  style="margin-bottom: 0;">
                    <button type="button" class="btn btn-primary" onclick="outExcel()">导出</button>
                    <button type="submit" class="btn btn-primary submit-btn">查询</button>
                    <div class="dropdown open" style="display: inline-block">
                        <button type="button" class="btn btn-primary dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown">筛选<span class="caret"></span></button>
                        <ul id="menuList" class="dropdown-menu pull-right" role="menu" aria-labelledby="dropdownMenu1" style="padding:10px;"></ul>
                    </div>
                </div>
                <div class="col-xs-4 text-right text-danger bloder">
                    合计金额：&#165;<?php echo ($sum_money); ?> &nbsp; 合计毛利：&#165;<?php echo ($profit); ?>
                </div>
            </form>
        </div>
        <table id="table" class="table table-striped table-bordered table-hover table-condensed mt-15">
            <thead>
                <tr>
                    <th>订单信息</th>
                    <th>成团时间</th>
                    <th>该团总人数</th>
                    <th>附加产品收入</th>
                    <th>营业额</th>
                    <th>总成本</th>
                    <!-- <th>附加产品成本</th> -->
                    <th>毛利</th>
                    <th>毛利率</th>
                </tr>
            </thead>
            <tbody>
                <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?><tr>
                        <td>
                            团号：<?php echo ($v["group_num"]); ?><br>
                            线路名称：<?php echo ($v["line_name"]); ?>
                            <?php if($v['platform'] == 1): ?>(自营)
                            <?php elseif($v['platform'] == 2): ?>(非自营)<?php endif; ?>
                        </td>
                        <td><?php echo ($v['group_time']); ?></td>
                        <td><?php echo ($v['sum(total_num)']); ?></td>
                        <!-- <td>
                            成人：<br>
                            儿童：<br>
                            老人：
                        </td> -->
                        <td><?php echo ($v['add_income']); ?></td>
                        <td><?php echo ($v['sum(need_pay)']); ?></td>
                        <td><?php echo ($v['closing_total_money']); ?></td>
                        <td><?php echo ($v['profit']); ?></td>
                        <td><?php echo ($v['profit_precent']); ?></td>
                    </tr><?php endforeach; endif; else: echo "" ;endif; ?>

            </tbody>
        </table>
    </div>
    <div class="modal fade" id="bjy-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times;</button>
                    <h4 class="modal-title" id="myModalLabel"> 外币结算</h4></div>
                <div class="modal-body">
                    <form id="bjy-form" class="form-inline" action="<?php echo U('Finance/singleForeignSettlement');?>" method="post">
                        <input class="form-control" type="hidden" name="order_id" id="order_id">
                        <table class="table table-striped table-bordered table-hover table-condensed">
                            <tr>
                                <th width="17%">结算金额：</th>
                                <td><span class="total_money"></span></td>
                            </tr>
                            <tr>
                                <th>汇率：</th>
                                <td><input class="form-control" class="input class="form-control"-medium" type="text" id="settlement_rate" name="settlement_rate" placeholder="请输入汇率"></td>
                            </tr>
                            <tr>
                                <th>实收金额：</th>
                                <td><input class="form-control" class="input class="form-control"-medium" type="text" id="end_foreign_money" name="end_foreign_money" value="0" placeholder="请输入实际收取的金额"><span class="currency_type"></span></td>
                            </tr>
                            <tr>
                                <th>实收人民币：</th>
                                <td><input class="form-control" class="input class="form-control"-medium" readonly type="text" name="end_need_pay"></td>
                            </tr>
                            <tr>
                                <th></th>
                                <td><input class="form-control" class="btn btn-success" type="submit" value="修改"></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
 <ul class="pagination pull-right"><?php echo ($show); ?></ul>


<script src="/Tpl/Operator/js/jedate/jedate.js"></script>
<script>
    //导出excel
    function outExcel(){
        $form = $('#settlement-form');
        $form.attr('action', '<?php echo U('outBusinessStatement');?>');
        $form.submit();
        $form.attr('action', '');
    }

    /**
     * 绑定日期选择器
     * @param  {[obj]}    obj        [元素]
     * @param  {[string]} dateFormat [时间格式]
     */
    function dateFormat(obj, dateFormat){
        jeDate({
            dateCell: '#'+$(obj).attr('id'),
            format: dateFormat,
            isinitVal:false,
            isTime:true, //isClear:false,
            okfun:function(val){
            }
        });
    }
   dateFormat($('#start'), 'YYYY-MM-DD');
   dateFormat($('#end'), 'YYYY-MM-DD');

     //生成筛选菜单
    (function(){
        var str = '';
        $('#table').find('tr').eq(0).find('th').each(function(i, n){
            str += '<li><input type="checkbox" class="filtrate" checked="" value="'+i+'" id="filtrate_'+i+'"><label for="filtrate_'+i+'">'+$(n).html()+'</label></li>';
        })
        $('#menuList').append(str);
    }())

    $('#menuList').on('change', '.filtrate', function(){
        var index = $(this).val();
        if($(this).prop('checked')){
            $('#table tr').each(function(i,n){  $(n).children().eq(index).show(); })
        }else{
            $('#table tr').each(function(i,n){  $(n).children().eq(index).hide();})
        }
    })
</script>