<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
    <link rel="stylesheet" href="/Public/statics/layui/css/layui.css"  media="all">
    <style>
        .newMess th{text-align:center;}
        th,td{padding: 10px;}
        .table thead>tr>th, .table tbody>tr>th, .table tfoot>tr>th, .table thead>tr>td, .table tbody>tr>td, .table tfoot>tr>td{vertical-align: middle}
    </style>

</head>
<body>

    <div class="page-header"><h1><i class="fa fa-home"></i> 首页>商家管理>添加单品供应商</h1></div>
     <div class="container-fluid">
         <form action="" method="post">
             <input type="hidden" name="supplier_id" value="<?php echo ($supplier_id); ?>">
         <table class="table table-striped table-bordered table-hover table-condensed text-center">
             <tbody>
             <tr>
                 <td style="width:25%;"><b> <span class="required">*</span>供应商名称</b></td>
                 <td class="text-left">
                     <input type="text" name="supplier_name" placeholder="供应商名称" value="<?php echo ($supplierInfo["supplier_name"]); ?>" required />
                 </td>
             </tr>
             <tr>
                 <td style="width:25%;"><b> <span class="required">*</span>供应商类型</b></td>
                 <td class="text-left">
                     <select name="type" id="" required="required">
                         <option <?php if($supplierInfo['type'] == 1): ?>selected<?php endif; ?> value="1">交通供应商</option>
                         <option <?php if($supplierInfo['type'] == 2): ?>selected<?php endif; ?> value="2">酒店供应商</option>
                         <option <?php if($supplierInfo['type'] == 3): ?>selected<?php endif; ?> value="3">保险供应商</option>
                         <option <?php if($supplierInfo['type'] == 4): ?>selected<?php endif; ?> value="4">景区供应商</option>
                     </select>
                 </td>
             </tr>
             <tr>
                 <td style="width:25%;"><b> <span class="required">*</span>使用币种</b></td>
                 <td class="text-left">
                     <select name="currency_id" id="" required="required">
                        <?php if(is_array($currency)): $i = 0; $__LIST__ = $currency;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if(is_numeric($key)): ?><option value="<?php echo ($key); ?>" <?php if($supplierInfo['currency_id'] == $key): ?>selected<?php endif; ?> ><?php echo ($vo); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                     </select>
                 </td>
             </tr>
             <tr>
                 <td><b> <span class="required">*</span>联系人</b></td>
                 <td class="text-left">
                     <input type="text" name="Linkman" placeholder="请输入联系人" value="<?php echo ($supplierInfo["Linkman"]); ?>" required />
                 </td>
             </tr>
             <tr>
                 <td><b> <span class="required">*</span>联系电话</b></td>
                 <td class="text-left">
                     <input type="text" name="Linkman_phone" placeholder="联系电话" maxlength="11" value="<?php echo ($supplierInfo["Linkman_phone"]); ?>" required />
                 </td>
             </tr>
             <tr>
                 <td><b> <span class="required">*</span>编号</b></td>
                 <td class="text-left">
                     <input type="text" name="supplier_sn" readonly placeholder="编号" value="<?php echo ($shopSn); ?>" required />
                 </td>
             </tr>
             <tr>
                 <td><b> <span class="required">*</span>合作时间</b></td>
                 <td class="text-left">
                     <input type="text" id="start_time" name="start_time" readonly placeholder="合作开始时间" value="<?php echo ($supplierInfo["start_time"]); ?>" required />
                      至
                     <input type="text" id="end_time" name="end_time" readonly placeholder="合作结束时间" value="<?php echo ($supplierInfo["end_time"]); ?>" required />
                 </td>
             </tr>
             <tr>
                 <td style="width:25%;"><b> <span class="required">*</span>所在地</b></td>
                 <td class="text-left">
                     国家
                     <select name="country" id="ar_country_id"  onchange="getProvince(this,'ar')" required>
                         <option value="">==选择国家==</option>
                         <?php if(is_array($country)): $i = 0; $__LIST__ = $country;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($supplierInfo['country'] == $vo['areaId']): ?>selected<?php endif; ?>  value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                     </select>
                     省
                     <select name="province" id="ar_province_id"  onchange="getCity(this,'ar')" required>
                         <option value="">==选择省==</option>
                         <?php if(is_array($province)): $i = 0; $__LIST__ = $province;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($supplierInfo['province'] == $vo['areaId']): ?>selected<?php endif; ?>  value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                     </select>
                     市
                     <select name="city" id="ar_city_id"  onchange="getArea(this,'ar')" required>
                         <option value="">==选择市==</option>
                         <?php if(is_array($city)): $i = 0; $__LIST__ = $city;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($supplierInfo['city'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                     </select>
                     区
                     <select name="area"   id="ar_area_id"  required>
                         <option value="">==选择区==</option>
                         <?php if(is_array($area)): $i = 0; $__LIST__ = $area;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($supplierInfo['area'] == $vo['areaId']): ?>selected<?php endif; ?>   value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                     </select>

                 </td>
             </tr>
             <tr>
                 <td colspan="2">
                     <?php if(checkAuth('Operator/Shops/addShops')): ?><input type="submit" class="btn btn-primary" value="保存"><?php endif; ?>
                 </td>
             </tr>
             </tbody>
         </table>
         </form>
     </div>

<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

    <script src="/Public/statics/layui/layui.all.js"></script>
    <script src="/Public/statics/layui/layui.js"></script>
    <script>
        layui.use('laydate', function() {
            var laydate = layui.laydate;
            //常规用法
            laydate.render({
                elem: '#start_time'
            });
            laydate.render({
                elem: '#end_time'
            });
        });

    </script>

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>