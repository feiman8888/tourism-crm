/**
 * Created by Administrator on 2017/9/6.
 */

$(function () {
    $(document).ajaxStart(function () {
        layer.load(1, {
            shade: [0.1, '#fff'] //0.1透明度的白色背景
        });
    });
    $(document).ajaxStop(function () {
        setTimeout(function () {
            layer.closeAll();
        },1000);
    });
});



//ajax上传附件
function ajax_upload_file(file, _this) {

    var formData = new FormData();
    formData.append("file", file[0]);
    $.ajax({
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        url: '/index.php?m=Dealer&c=Base&a=ajax_upload_file',
        dataType: 'json',
        success: function (data) {
            if (data.status==1) {

                $(_this).prev().val(data.path);

                $(_this).parent().next().attr('src', data.path);
            }else{
                layer.msg(data.info);
            }
        }
    });
}


/**
 * 根据省份获取市区
 */
function getProvince(_this,prefix) {
    var country_id=$(_this).find(':selected').val();
    $("#"+prefix+"_province_id").html('<option value="">==选择省==</option>');
    $("#"+prefix+"_city_id").html('<option value="">==选择市==</option>');
    $("#"+prefix+"_area_id").html('<option value="">==选择区==</option>');
    $.ajax({
        type: 'POST',
        data: {country_id:country_id},
        url: '/index.php?m=Dealer&c=Platform&a=getProvince',
        dataType: 'json',
        success: function (data) {
            var html='';
            for(var i in data){
                html+=" <option value='"+data[i].areaId+"'>"+data[i].areaName+"</option>";
            }
            $("#"+prefix+"_province_id").append(html);
        }
    });
}


/**
 * 根据省份获取市区
 */
function getCity(_this,prefix) {

    var province_id=$(_this).find(':selected').val();
    $("#"+prefix+"_city_id").html('<option value="">==选择市==</option>');
    $("#"+prefix+"_area_id").html('<option value="">==选择区==</option>');
    $.ajax({
        type: 'POST',
        data: {province_id:province_id},
        url: '/index.php?m=Dealer&c=Platform&a=getCity',
        dataType: 'json',
        success: function (data) {
           var html='';
           for(var i in data){
               html+=" <option value='"+data[i].areaId+"'>"+data[i].areaName+"</option>";
           }
           $("#"+prefix+"_city_id").append(html);
        }
    });
}

/**
 * 根据市获取区
 */
function getArea(_this,prefix) {

    var city_id=$(_this).find(':selected').val();
    $("#"+prefix+"_area_id").html('<option value="">==选择区==</option>');
    $.ajax({
        type: 'POST',
        data: {city_id:city_id},
        url: '/index.php?m=Dealer&c=Platform&a=getArea',
        dataType: 'json',
        success: function (data) {
            var html='';
            for(var i in data){
                html+=" <option value='"+data[i].areaId+"'>"+data[i].areaName+"</option>";
            }
            $("#"+prefix+"_area_id").append(html);
        }
    });
}

/**
 * 更改城市的锁定和启用状态
 */
function changeIsShow(areaId,status) {
    $.ajax({
        type: 'POST',
        data: {areaId:areaId,isShow:status},
        url: '/index.php?m=Dealer&c=City&a=changeIsShow',
        dataType: 'json',
        success: function (data) {
            layer.msg(data.msg);
            if(data.status==1){
                setTimeout(function(){
                    location.reload();
                },1500);
            }
        }
    });
}


/**
 * 限制只能输入整数或者带2位小数的
 */

function clearNoNum(obj)
{
    //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d.]/g,"");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,"");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
}