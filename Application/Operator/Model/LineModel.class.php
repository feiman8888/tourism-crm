<?php
namespace Operator\Model;
/**
 * 权限规则model
 */
class AuthGroupAccessModel extends BaseModel{

    protected $tableName = 'dealer_auth_group_access';

    /**
	 * 根据group_id获取全部用户id
	 * @param  int $group_id 用户组id
	 * @return array         用户数组
	 */
	public function getUidsByGroupId($group_id){
		$user_ids=$this
			->where(array('group_id'=>$group_id))
			->getField('uid',true);
		return $user_ids;
	}

	/**
	 * 获取管理员权限列表
	 */
	public function getAllData(){

	    //区分哪一个平台的会员
        $userInfo=session('dealer_user');
        if($userInfo['pid']==0){
            $map['u.pid'] = $userInfo['operator_id'];
            $map['u.operator_id'] = $userInfo['operator_id'];
            $map['_logic'] = 'OR';
        }else{
            $map['u.pid'] = $userInfo['pid'];
            $map['u.operator_id'] = $userInfo['pid'];
            $map['_logic'] = 'OR';
        }

        //u.email,
		$data=$this
			->field('u.operator_id as id,u.operator_account as username,aga.group_id,ag.title')
			->alias('aga')
            ->where($map)
			->join('__OPERATOR__ u ON aga.uid=u.operator_id','RIGHT')
			->join('__DEALER_AUTH_GROUP__ ag ON aga.group_id=ag.id','LEFT')
			->select();
		// 获取第一条数据
		$first=$data[0];
		$first['title']=array();
		$user_data[$first['id']]=$first;
		// 组合数组
		foreach ($data as $k => $v) {
			foreach ($user_data as $m => $n) {
				$uids=array_map(function($a){return $a['id'];}, $user_data);
				if (!in_array($v['id'], $uids)) {
					$v['title']=array();
					$user_data[$v['id']]=$v;
				}
			}
		}
		// 组合管理员title数组
		foreach ($user_data as $k => $v) {
			foreach ($data as $m => $n) {
				if ($n['id']==$k) {
					$user_data[$k]['title'][]=$n['title'];
				}
			}
			$user_data[$k]['title']=implode('、', $user_data[$k]['title']);
		}
		// 管理组title数组用顿号连接
		return $user_data;

	}


}
