<?php
namespace Reseller\Model;
/**
 * 积分模型
 */
class ScoreModel extends BaseModel{

    // protected $tableName = 'public_visitor_info';
    protected $_validate = [
        array('amount','number','消费金额必须为数字!',1,'',3), // 验证字段必填
        array('score','number','积分数额需为数字!',1,'',3), // 验证字段必填

    ];
    /**
     * 获取积分设置
     */
    public function getScoreCofing(){
        $m = M('public_score_configs');
        $reseller_id = session('reseller_user.pid')==0?session('reseller_user.reseller_id'):session('reseller_user.pid');
        $scoreInfo = $m->where(array('reseller_id'=>$reseller_id))->find();
        if(!$scoreInfo){
            $id = $m->add(['reseller_id' => $reseller_id]);
            unset($scoreInfo);
            $scoreInfo = [
                'id' => $id,
                'amount' => 0.00,
                'score' => 0,
                'reseller_id' => $reseller_id,
            ];

        }
        return $scoreInfo;
    }

    /**
     * 修改积分设置
     */
    public function editScoreCofing($data){
        // 对data数据进行验证
        if(!$data=M('public_score_configs')->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $map['id']=$data['id'];
            unset($data['id']);
            $result=M('public_score_configs')
                ->where(array($map))
                ->save($data);
            // echo M()->_Sql();die;
            return $result;
        }

    }
}

